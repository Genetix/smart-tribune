<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 02/04/2020
 */


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QuestionAnwserControllerTest extends WebTestCase
{
	/**
	 *    This method should test insert action
	 */
	public function testQuestionInsertAction(): void
	{
		
		$client = static::createClient();
		
		//test insertion
		$client->request('POST', '/insert', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson());
		
		$this->assertEquals(201, $client->getResponse()->getStatusCode());
		
		//bad enum for answer.channel.
		$client->request('POST', '/insert', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson('enum'));

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
		
		//Bad html method
		$client->request('GET', '/insert', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson());
		
		$this->assertEquals(405, $client->getResponse()->getStatusCode());
		
	}
	
	
	/**
	 *    This method should test update action
	 */
	public function testQuestionUpdateAction(): void
	{

		$client = static::createClient();

		//test update ok
		$client->request('PATCH', '/update/1', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson('update'));
		$this->assertEquals(200, $client->getResponse()->getStatusCode());
		
		//bad enum for answer.channel.
		$client->request('PATCH', '/update/1', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson('enum'));
		$this->assertEquals(400, $client->getResponse()->getStatusCode());
		
		//id question not exist
		$client->request('PATCH', '/update/56161', [], [], ['CONTENT_TYPE' => 'application/json'], $this->getJson('update'));
		$this->assertEquals(304, $client->getResponse()->getStatusCode());

	
	}
	
	/**
	 * this method constructs json to test all testcase
	 *
	 * @param null $editJson
	 *
	 * @return mixed|string
	 */
	public function getJson($editJson = null)
	{
		$json = '{"answers":[{"body":"text body","channel":"bot"},{"body":"text body 2","channel":"faq"}],"title":"title question","promoted":true,"status":"draft","created":"25-03-2020 19:20:30","updated":"25-03-2020 19:20:32"}';
		
		$json = json_decode($json, true);
		
		if ($editJson === 'enum') {
			$json['answers'][0]['channel'] = 'bott';
		}
		
		if ($editJson === 'update'){
			$json['title'] = 'title updated';
		}
		
		return json_encode($json);
	}
}