<?php

use PHPUnit\Framework\TestCase;

/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 25/03/2020
 */
class ExporterTest extends TestCase
{
	public function export_data_to_csv()
	{
		$calculator = new Calculator();
		$result = $calculator->add(30, 12);
		
		// assert that your calculator added the numbers correctly!
		$this->assertEquals(42, $result);
	}
}