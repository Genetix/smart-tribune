<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 22/03/2020
 */


namespace App\Controller;


use App\Entity\Question;
use App\Entity\QuestionHistoric;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\DriverException;
use ErrorException;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuestionAnswerController extends AbstractController
{
	/**
	 * This method save a new question anwser
	 * @Route("/insert", name="question_create", methods={"POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request                 $request
	 * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function QuestionInsertAction(Request $request, ValidatorInterface $validator): Response
	{
		
		//Get content of post
		$data = $request->getContent();
		try {
			//serialize payload to entity
			$question = $this->get('serializer')->deserialize($data, Question::class, 'json');
			
		} catch (InvalidArgumentException $e) {
			return new Response('Answers.channel value is restricted to "faq" or "bot" && Status value is restricted to "draft" or "published", verify your payload', Response::HTTP_BAD_REQUEST);
		} catch (NotEncodableValueException | NotNormalizableValueException $e) {
			return new Response('Syntax payload error', Response::HTTP_BAD_REQUEST);
		}
		
		//Validate question
		$errors = $validator->validate($question);
		if (count($errors) > 0) {
			return new Response('Validation error verify payload (Answers.channel, Question.status, ...)', Response::HTTP_BAD_REQUEST);
		}
		
		//persist question
		$em = $this->getDoctrine()->getManager();
		$em->persist($question);
		$em->flush();
		
		return new Response('', Response::HTTP_CREATED);
		
	}
	
	/**
	 * This method update Question title or/and status
	 * @Route("/update/{id}", name="question_update", methods={"PATCH"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request                 $request
	 * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function QuestionUpdateAction(Request $request, ValidatorInterface $validator): Response
	{
		
		
		$id = $request->get('id');
		
		$em       = $this->getDoctrine()->getManager();
		$question = $this->getDoctrine()->getRepository(Question::class)->find($id);
		
		//Question not found in database
		if ($question === null) {
			return new Response('', Response::HTTP_NOT_MODIFIED);
		}
		
		//Get content of post
		$data = $request->getContent();
		
		try {
			//serialize payload to entity
			$questionRequest = $this->get('serializer')->deserialize($data, Question::class, 'json');
			
		} catch (InvalidArgumentException $e) {
			return new Response('Answers.channel value is restricted to "faq" or "bot" && Status value is restricted to "draft" or "published", verify your payload', Response::HTTP_BAD_REQUEST);
		} catch (NotEncodableValueException | NotNormalizableValueException $e) {
			return new Response('Syntax payload error', Response::HTTP_BAD_REQUEST);
		}
		
		//Validate question
		$errors = $validator->validate($questionRequest);
		
		if (count($errors) > 0) {
			return new Response('Validation error verify payload (Answers.channel, Question.status, ...)', Response::HTTP_BAD_REQUEST);
		}
		
		//update attribute
		$question->setStatus($questionRequest->getStatus());
		$question->setTitle($questionRequest->getTitle());
		$updateTime = new \DateTime('now');
		$question->setUpdated($updateTime);
		
		$em->flush();
		
		//Create question historic
		$this->createQuestionHistoric($question);
		
		return new Response('', Response::HTTP_OK);
		
	}
	
	/**
	 * This method save an historic for a question
	 *
	 * @param \App\Entity\Question $question
	 *
	 * @throws \Exception
	 */
	public function createQuestionHistoric(Question $question): void
	{
		$em = $this->getDoctrine()->getManager();
		
		$questionHistoric = new QuestionHistoric();
		$questionHistoric->setTitle($question->getTitle());
		$questionHistoric->setStatus($question->getStatus());
		$questionHistoric->setUpdated($question->getUpdated());
		$questionHistoric->setQuestion($question);
		
		$em->persist($questionHistoric);
		$em->flush();
	}
	
}