<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 23/03/2020
 */


namespace App\Command;

use App\Entity\QuestionHistoric;
use App\Model\Exporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends Command
{
	
	private $em;
	protected static $defaultName = 'question_historic_backup';
	
	public function __construct(EntityManagerInterface $entityManager)
	{
		parent::__construct();
		$this->em = $entityManager;
	}
	
	
	protected function configure(): void
	{
		$this->addArgument('filename', InputArgument::OPTIONAL, 'name your file', 'export');
		$this->addArgument('delimiter', InputArgument::OPTIONAL, 'set a delimiter for csv', '|');
		$this->addArgument('enclosure', InputArgument::OPTIONAL, 'set an enclosure for csv', '"');
	}
	
	/**
	 * @param \Symfony\Component\Console\Input\InputInterface   $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 *
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$result = $this->em->getRepository(QuestionHistoric::class)->findAll();
		
		$result = prepareArrayToCsv($result);
		
		//call exporter and method to create csv
		$export = new Exporter($result, $input->getArgument('filename'), $input->getArgument('delimiter'), $input->getArgument('enclosure'));
		$export->export_data_to_csv();
		
		$output->writeln([
			'csv is create',
		]);
		
		return 0;
	}
	

	
	
}