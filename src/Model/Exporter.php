<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 23/03/2020
 */


namespace App\Model;


class Exporter
{
	/**
	 * @var array
	 */
	private $data;
	/**
	 * @var string
	 */
	private $filename;
	/**
	 * @var string
	 */
	private $delimiter;
	/**
	 * @var string
	 */
	private $enclosure;
	
	/**
	 * Exporter constructor.
	 *
	 * @param $data
	 * @param $filename
	 * @param $delimiter
	 * @param $enclosure
	 */
	public function __construct($data, $filename, $delimiter, $enclosure)
	{
		$this->data      = $data;
		$this->filename  = $filename;
		$this->delimiter = $delimiter;
		$this->enclosure = $enclosure;
	}
	
	/**
	 * this method write a csv file
	 */
	public function export_data_to_csv(): void
	{

		// I open PHP memory as a file
		$fp = fopen($this->filename.'.csv', 'w');
		
		// Insert the UTF-8 BOM in the file
		fwrite($fp, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
		
		// Add all the data in the file
		foreach ($this->data as $fields) {
			fputcsv($fp, $fields, $this->delimiter, $this->enclosure);
		}
		
		// Close the file
		fclose($fp);
		
	}
	
	
	
	
}

