<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnswerRepository")
 */
class Answer
{
	
	private const CHANNEL_BOT = 'bot';
	private const CHANNEL_FAQ = 'faq';
	
	
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @Assert\NotBlank
	 * @Assert\Choice({"bot", "faq"})
	 * @ORM\Column(type="string", columnDefinition="ENUM('bot', 'faq')", length=255)
	 */
	private $channel;
	
	/**
	 * @Assert\NotBlank
	 * @ORM\Column(type="string", length=255)
	 */
	private $body;
	
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="answers", cascade={"all"}, fetch="EAGER")
	 */
	private $question;
	
	
	/**
	 * @return mixed
	 */
	public function getBody()
	{
		return $this->body;
	}
	
	/**
	 * @param mixed $body
	 *
	 * @return Answer
	 */
	public function setBody($body): Answer
	{
		$this->body = $body;
		
		return $this;
	}
	
	
	public function setQuestion(?Question $question): self
	{
		$this->question = $question;
		
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getChannel()
	{
		return $this->channel;
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @param mixed $channel
	 *
	 * @return Answer
	 */
	public function setChannel($channel)
	{
		$this->channel = $channel;
		
		return $this;
	}
	
	
}
