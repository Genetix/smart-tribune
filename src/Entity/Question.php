<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 22/03/2020
 */


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Question
{
	
	private const STATUS_DRAFT = 'draft';
	private const STATUS_PUBLISHED = 'published';
	
	
	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @Assert\NotBlank
	 * @Assert\Type("string")
	 * @var string
	 * @ORM\Column(type="string",nullable=false, length=100)
	 */
	private $title;
	
	/**
	 * @Assert\NotBlank
	 * @Assert\Type("bool")
	 * @var boolean
	 * @ORM\Column(type="boolean",nullable=false, length=100)
	 */
	private $promoted;
	
	/**
	 * @Assert\NotBlank
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	private $created;
	
	/**
	 * @Assert\NotBlank
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	private $updated;
	
	/**
	 * @Assert\NotBlank
	 * @Assert\Choice({"draft", "published"})
	 * @var string
	 * @ORM\Column(type="string", columnDefinition="ENUM('draft', 'published')")
	 */
	private $status;
	
	/**
	 *
	 * @Assert\Valid
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\Answer", mappedBy="question", cascade={"all"}, fetch="EAGER")
	 */
	private $answers;
	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\QuestionHistoric", mappedBy="question", orphanRemoval=true)
	 */
	private $questionHistorics;
	
	public function __construct()
	{
		$this->answers           = new ArrayCollection();
		$this->questionHistorics = new ArrayCollection();
	}

	
	/**
	 * @param $status
	 *
	 * @return \App\Entity\Question
	 */
	public function setStatus($status): Question
	{
		if (!in_array($status, array(self::STATUS_PUBLISHED, self::STATUS_DRAFT), true)) {
			throw new \InvalidArgumentException('Invalid status');
		}
		$this->status = $status;
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @param int $id
	 *
	 * @return Question
	 */
	public function setId(int $id): Question
	{
		$this->id = $id;
		
		return $this;
	}
	
	/**
	 * @param string $title
	 *
	 * @return Question
	 */
	public function setTitle(string $title): Question
	{
		$this->title = $title;
		
		return $this;
	}
	
	/**
	 * @param bool $promoted
	 *
	 * @return Question
	 */
	public function setPromoted(bool $promoted): Question
	{
		$this->promoted = $promoted;
		
		return $this;
	}
	
	
	/**
	 * @return Collection|Answer[]
	 */
	public function getAnswers(): Collection
	{
		return $this->answers;
	}
	
	public function addAnswer(Answer $answer): self
	{
		if (!$this->answers->contains($answer)) {
			$this->answers[] = $answer;
			$answer->setQuestion($this);
		}
		
		return $this;
	}
	
	public function removeAnswer(Answer $answer): self
	{
		if ($this->answers->contains($answer)) {
			$this->answers->removeElement($answer);
			// set the owning side to null (unless already changed)
			if ($answer->getQuestion() === $this) {
				$answer->setQuestion(null);
			}
		}
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}
	
	
	/**
	 * @return bool
	 */
	public function isPromoted(): bool
	{
		return $this->promoted;
	}
	
	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}
	
	/**
	 * @param \DateTime $created
	 *
	 * @return Question
	 */
	public function setCreated(\DateTime $created): Question
	{
		$this->created = $created;
		
		return $this;
	}
	
	/**
	 * @param \DateTime $updated
	 *
	 * @return Question
	 */
	public function setUpdated(\DateTime $updated): Question
	{
		$this->updated = $updated;
		
		return $this;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $answers
	 *
	 * @return Question
	 */
	public function setAnswers(\Doctrine\Common\Collections\ArrayCollection $answers): Question
	{
		$this->answers = $answers;
		
		return $this;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getUpdated(): \DateTime
	{
		return $this->updated;
	}
	
	/**
	 * @return Collection|QuestionHistoric[]
	 */
	public function getQuestionHistorics(): Collection
	{
		return $this->questionHistorics;
	}
	
	public function addQuestionHistoric(QuestionHistoric $questionHistoric): self
	{
		if (!$this->questionHistorics->contains($questionHistoric)) {
			$this->questionHistorics[] = $questionHistoric;
			$questionHistoric->setQuestion($this);
		}
		
		return $this;
	}
	
	public function removeQuestionHistoric(QuestionHistoric $questionHistoric): self
	{
		if ($this->questionHistorics->contains($questionHistoric)) {
			$this->questionHistorics->removeElement($questionHistoric);
			// set the owning side to null (unless already changed)
			if ($questionHistoric->getQuestion() === $this) {
				$questionHistoric->setQuestion(null);
			}
		}
		
		return $this;
	}
	
	
}