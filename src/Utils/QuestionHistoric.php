<?php
/**
 * Created by IntellijIdea
 * User: QRoussillon
 * Date: 25/03/2020
 */


/**
 * This method return an array with all information to export in csv for question Historic
 *
 * @param $array
 *
 * @return array
 */
function prepareArrayToCsv($array): array
{
	$arrayResult   = [];
	$arrayResult[] = ['id', 'question_id', 'title', 'status', 'updated'];
	foreach ($array as $questionHistoric) {
		$arrayResult[] = [
			$questionHistoric->getId(),
			$questionHistoric->getQuestion()->getId(),
			$questionHistoric->getTitle(),
			$questionHistoric->getStatus(),
			$questionHistoric->getUpdated()->format('Y-m-d H:m:s')
		];
	}
	
	return $arrayResult;
}