-Smart tribune test

### Installing

```
composer update && composer install
```

connect database in .env file

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
```

###After that you can use this project

```
route: 
- /insert POST method

with data:
{
  "answers": [
    {
      "body": "text body",
      "channel": "bot"
    },
    {
      "body": "text body 2",
      "channel": "faq"
    }
  ],
  "title": "title question",
  "promoted": true,
  "status": "draft",
  "created": "25-03-2020 19:20:30",
  "updated": "25-03-2020 19:20:32"
}

- /update/{id}  PATCH method
{
  "answers": [
    {
      "body": "text body",
      "channel": "bot"
    }
  ],
  "title": "title question new",
  "promoted": true,
  "status": "published",
  "created": "25-03-2020 19:20:30",
  "updated": "25-10-2020 19:20:32"
}
```